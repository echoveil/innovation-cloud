Rails.application.routes.draw do

  get '/thanks' => 'pages#thanks'
  resources :signups
  root 'signups#new'
end
